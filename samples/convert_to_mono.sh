for file in *.wav; do
  name=$(echo $file | tr -d ' ')
  sox "$file" MONO$name remix 1
done
